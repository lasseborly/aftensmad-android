#Aftensmad - Android

######A food recipe app that takes a more casual approach to choosing your dinner. 

You are standing in the mall with your friends and have no idea what to cook for dinner. You have eaten pasta bolognese and burgers the last week and your imagination have run dry already. Whip out Aftensmad and start swiping for your next dinner!

##Technology

* ###[Retrofit](http://square.github.io/retrofit/)
     
    A REST client for Android used to accelerate development.    
***

* ###[Swipecards](https://github.com/Diolor/Swipecards)
     
    A Tinder like swipe able card effect. works great for casual glancing and impulsive lust from the user. 
***