package com.lasseborly.aftensmad.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.lasseborly.aftensmad.ArrayAdapters.IngredientsArrayAdapter;
import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.R;


public class IngredientsActivity extends Activity {

    private ListView ingredientsList;
    private IngredientsArrayAdapter ingredientsArrayAdapter;
    private Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients);

        recipe = (Recipe) getIntent().getSerializableExtra(MainActivity.RECIPE);

        ingredientsArrayAdapter = new IngredientsArrayAdapter(this, R.layout.ingredient_item, recipe.getIngredients());
        ingredientsList = (ListView) findViewById(R.id.ingredients_list);
        ingredientsList.setAdapter(ingredientsArrayAdapter);

    }
}
