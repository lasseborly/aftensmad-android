package com.lasseborly.aftensmad.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.lasseborly.aftensmad.ArrayAdapters.TipsArrayAdapter;
import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.R;


public class TipsActivity extends Activity {

    private ListView tipsList;
    private TipsArrayAdapter tipsArrayAdapter;
    private Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        recipe = (Recipe) getIntent().getSerializableExtra(MainActivity.RECIPE);

        tipsArrayAdapter = new TipsArrayAdapter(this, R.layout.tip_item, recipe.getTips());
        tipsList = (ListView) findViewById(R.id.steps_list);
        tipsList.setAdapter(tipsArrayAdapter);

    }
}
