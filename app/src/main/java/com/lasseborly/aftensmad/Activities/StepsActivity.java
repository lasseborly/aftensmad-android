package com.lasseborly.aftensmad.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.lasseborly.aftensmad.ArrayAdapters.StepsArrayAdapter;
import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.R;


public class StepsActivity extends Activity {

    private ListView stepsList;
    private StepsArrayAdapter stepsArrayAdapter;
    private Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps);

        recipe = (Recipe) getIntent().getSerializableExtra(MainActivity.RECIPE);

        stepsArrayAdapter = new StepsArrayAdapter(this, R.layout.step_item, recipe.getSteps());
        stepsList = (ListView) findViewById(R.id.steps_list);
        stepsList.setAdapter(stepsArrayAdapter);

    }
}
