package com.lasseborly.aftensmad.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lasseborly.aftensmad.ArrayAdapters.IngredientsArrayAdapter;
import com.lasseborly.aftensmad.Models.Ingredient;
import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.Models.Step;
import com.lasseborly.aftensmad.Models.Tip;
import com.lasseborly.aftensmad.R;
import com.lasseborly.aftensmad.ArrayAdapters.TipsArrayAdapter;


public class RecipeActivity extends Activity {

    private Recipe recipe;

    private TextView recipeTitle, recipePrepare, recipeCooking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        recipe = (Recipe) getIntent().getSerializableExtra(MainActivity.RECIPE);



        //recipeTitle = (TextView) findViewById(R.id.recipe_title);
        //recipePrepare = (TextView) findViewById(R.id.recipe_prepare);
        //recipeCooking = (TextView) findViewById(R.id.recipe_cooking);

        //recipeTitle.setText(recipe.getTitle());
        //recipePrepare.setText(Integer.toString(recipe.getPrepare()) + " min");
        //recipeCooking.setText(Integer.toString(recipe.getCooking()) + " min");


    }

    private void displayActivity(Recipe recipe, Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(MainActivity.RECIPE, recipe);
        startActivity(intent);
    }

}
