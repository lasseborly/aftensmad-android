package com.lasseborly.aftensmad.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lasseborly.aftensmad.Models.Ingredient;
import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.Models.Step;
import com.lasseborly.aftensmad.Models.Tip;
import com.lasseborly.aftensmad.R;
import com.lasseborly.aftensmad.ArrayAdapters.RecipeArrayAdapter;
import com.lasseborly.aftensmad.RecipesAPI;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {

    private ArrayList<Recipe> recipesList;
    private ArrayAdapter<Recipe> recipeArrayAdapter;
    private int i;
    public final static String RECIPE = "recipe";

    public static final String ENDPOINT = "http://joinerjack.com/aftensmad";

    SwipeFlingAdapterView flingContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);

        Recipe newRecipe = new Recipe("Svitsede Tarme med ekstra hvidløg ved siden af");
        List<Step> steps = new ArrayList<>();
        steps.add(new Step("Svits", "Svits baconen sprødt. Læg det til afdrypning på et stykke bagepapir eller køkkenrulle."));
        steps.add(new Step("Svits", "Svits baconen sprødt. Læg det til afdrypning på et stykke bagepapir eller køkkenrulle."));
        newRecipe.setSteps(steps);

        newRecipe.setDescription("Svitset bacon er en super ret med dehlig smag og udemærket aroma der fylder svælget tilstrækkeligt. ");

        List<Tip> tips = new ArrayList<>();
        tips.add(new Tip("Erstatning", "Svinekød i tern kan erstattes med oksekød i tern."));
        tips.add(new Tip("Erstatning", "Svinekød i tern kan erstattes med oksekød i tern."));
        newRecipe.setTips(tips);

        newRecipe.setCooking(30);
        newRecipe.setPrepare(15);

        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(new Ingredient("Vandmeloner", 20.0, "stk"));
        ingredients.add(new Ingredient("Honningmeloner", 10.0, "stk"));

        newRecipe.setIngredients(ingredients);


        recipesList = new ArrayList<Recipe>();
        recipesList.add(newRecipe);

        SharedPreferences storage = getPreferences(MODE_PRIVATE);
        Editor editor = storage.edit();
        Gson gson = new Gson();
        String json = gson.toJson(recipesList);
        editor.putString("Recipes" ,json);
        editor.commit();

        recipeArrayAdapter = new RecipeArrayAdapter(this, R.layout.swipe_item, recipesList);

        flingContainer.setAdapter(recipeArrayAdapter);

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                recipesList.remove(0);
                recipeArrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                makeToast(MainActivity.this, "Left!");
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                makeToast(MainActivity.this, "Right!");
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here
                recipesList.add(new Recipe("Empty"));
                recipeArrayAdapter.notifyDataSetChanged();
                Log.d("LIST", "notified");
                i++;
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);    //left
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);      //right
            }
        });

        ImageButton buttonLeft = (ImageButton) findViewById(R.id.left);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectLeft();
            }
        });

        ImageButton buttonRight = (ImageButton) findViewById(R.id.right);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectRight();
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                displayRecipe(recipesList.get(itemPosition));
            }
        });

    }

    static void makeToast(Context ctx, String s){
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }

    private void displayRecipe(Recipe recipe) {
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra(RECIPE, recipe);
        startActivity(intent);
    }

    //Strukturen skal laves om?!
    private void getRecipes() {


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .build();

        RecipesAPI api = adapter.create(RecipesAPI.class);

        api.getAll(new Callback<ArrayList<Recipe>>() {
            @Override
            public void success(ArrayList<Recipe> recipes, Response response) {
                recipesList = recipes;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("ERROR", "Error");
            }
        });

    }

}
