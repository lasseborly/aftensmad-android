package com.lasseborly.aftensmad.Models;

import java.io.Serializable;

public class Ingredient implements Serializable {

    private String name;
    private double amount;
    private String scale;

    public Ingredient(String name,double amount, String scale) {
        this.name = name;
        this.amount = amount;
        this.scale = scale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", scale='" + scale + '\'' +
                '}';
    }
}
