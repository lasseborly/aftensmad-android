package com.lasseborly.aftensmad.Models;

import java.io.Serializable;

public class Tip implements Serializable {

    private String title;
    private String description;

    public Tip(){

    }

    public Tip(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
