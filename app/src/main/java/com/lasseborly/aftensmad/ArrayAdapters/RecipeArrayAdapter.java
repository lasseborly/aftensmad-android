package com.lasseborly.aftensmad.ArrayAdapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lasseborly.aftensmad.Models.Recipe;
import com.lasseborly.aftensmad.R;

import java.util.List;

public class RecipeArrayAdapter extends ArrayAdapter<Recipe> {

    private Context context;
    private List<Recipe> recipes;

    public RecipeArrayAdapter(Context context, int resource, List<Recipe> recipes) {
        super(context, resource, recipes);
        this.context = context;
        this.recipes = recipes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Recipe recipe = recipes.get(position);

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.swipe_item, parent, false);
        }

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(recipe.getTitle());

        return view;
    }
}
