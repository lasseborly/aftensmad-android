package com.lasseborly.aftensmad.ArrayAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lasseborly.aftensmad.Models.Step;
import com.lasseborly.aftensmad.Models.Tip;
import com.lasseborly.aftensmad.R;

import java.util.List;

public class TipsArrayAdapter extends ArrayAdapter<Tip> {

    private Context context;
    private List<Tip> tips;

    public TipsArrayAdapter(Context context, int resource, List<Tip> tips){
        super(context, resource, tips);
        this.context = context;
        this.tips = tips;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Tip tip = tips.get(position);

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.tip_item, parent, false);
        }

        TextView tipTitle = (TextView) view.findViewById(R.id.tip_title);
        TextView tipDescription = (TextView) view.findViewById(R.id.tip_description);

        tipTitle.setText(tip.getTitle());
        tipDescription.setText(tip.getDescription());

        return view;
    }

}
