package com.lasseborly.aftensmad.ArrayAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lasseborly.aftensmad.Models.Step;
import com.lasseborly.aftensmad.R;

import java.util.List;

public class StepsArrayAdapter extends ArrayAdapter<Step> {

    private Context context;
    private List<Step> steps;

    public StepsArrayAdapter(Context context, int resource, List<Step> steps){
        super(context, resource, steps);
        this.context = context;
        this.steps = steps;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        Step step = steps.get(position);

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.step_item, parent, false);
        }

        TextView stepTitle = (TextView) view.findViewById(R.id.step_title);
        TextView stepDescription = (TextView) view.findViewById(R.id.step_description);

        stepTitle.setText(step.getTitle());
        stepDescription.setText(step.getDescription());

        return view;
    }

}
