package com.lasseborly.aftensmad.ArrayAdapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lasseborly.aftensmad.Models.Ingredient;
import com.lasseborly.aftensmad.R;

import java.util.List;

public class IngredientsArrayAdapter extends ArrayAdapter<Ingredient> {

    private Context context;
    private List<Ingredient> ingredients;

    public IngredientsArrayAdapter(Context context, int resource, List<Ingredient> ingredients) {
        super(context, resource, ingredients);
        this.context = context;
        this.ingredients = ingredients;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Ingredient ingredient = ingredients.get(position);

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.ingredient_item, parent, false);
        }

        TextView ingredientAmount = (TextView) view.findViewById(R.id.ingredient_amount);
        TextView ingredientScale = (TextView) view.findViewById(R.id.ingredient_scale);
        TextView ingredientName = (TextView) view.findViewById(R.id.ingredient_name);

        ingredientAmount.setText(Double.toString(ingredient.getAmount()));
        ingredientScale.setText(ingredient.getScale());
        ingredientName.setText(ingredient.getName());

        return view;
    }
}
