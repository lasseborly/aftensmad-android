package com.lasseborly.aftensmad;

import com.lasseborly.aftensmad.Models.Recipe;

import java.util.ArrayList;


import retrofit.Callback;
import retrofit.http.GET;


public interface RecipesAPI {

    @GET("/recipes/recipes.json")
    public void getAll(Callback<ArrayList<Recipe>> response);

}
